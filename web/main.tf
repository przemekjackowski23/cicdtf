#ec2

resource "aws_instance" "server" {
    ami = "ami-08ba52a61087f1bd6"
    instance_type = "t2.micro"
    subnet_id = var.sn
    security_groups = [var.sg]

    tags = {
        Name = "myserver"
    } 
}